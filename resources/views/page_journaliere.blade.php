@extends('layouts.layout_journalier')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-2 aside">
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>
            <a class="btn btn-primary" href="{{ route('principale') }}">Retour à Joutra</a>

        </div>
        <div class="col-10">
            <div class="page">
                <section class="page-content">
                    <div class="row"> 
                        <div class="col-sm-12 page-title">
                            <h2 class="text-center">Journal de travaux</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 entrepreneur">
                            <div class="row">
                                <div class="col">
                                    <h2>Données de l'entrepreneur</h2>
                                    <hr>
                                </div>
                                <div class="col">
                                    <h2>Données du client</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 parametre">
                            <h2>Paramètre de la page journalière</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5 ouvrier">
                            <h2>Liste des ouvirers</h2>
                        </div>
                        <div class="col-7 travaux">
                            <h2>Liste des travaux executés</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 essais">
                            <h2>essais exécuté</h2>
                        </div>
                        <div class="col-8 machine">
                            <h2>Liste des machines</h2>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-8 commentaire">
                            <h2>Commentaire</h2>
                        </div>
                        <div class="col-4 materiaux">
                            <h2>Liste des matériaux utilisé</h2>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

@endsection