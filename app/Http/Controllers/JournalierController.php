<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JournalierController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    //View pour la création de la fiche journalière
    public function index($id){
        $test = $id;
        return view('new_site.journalier.journalier_create')->withTest($test);
        
        
    }
}
