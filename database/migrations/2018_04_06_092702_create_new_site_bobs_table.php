<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewSiteBobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_site_bobs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->text('nom_ouvrier')->nullable();
            $table->text('prenom_ouvrier')->nullable();
            $table->text('metier_ouvrier');
            $table->text('employeur_ouvrier')->nullable();
            $table->boolean('sous_traitant_ouvrier')->default(0)->nullable(); 
            $table->integer('user_id');          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_site_bobs');
    }
}
