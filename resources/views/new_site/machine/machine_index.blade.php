@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection


@section('content')

<div class="container-fluid">
@include('partials.new_site._new_site_nav')
<section class="">
    <div class="row"> 
        <div class="col">
                <div class="card content">
                    <div class="card-header">
                    <h5 class="text-center">Liste de vos machines</h5>
                    <a href="{{route('machines.create')}}" class="btn btn-success btn-add">Ajouter une machine</a>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    </div>

                    <table class="table">
                        <thead class="card-title text-center">
                            <tr>
                            <th scope="col">Type de machine</th>
                            {{-- <th scope="col">Marque</th>
                            <th scope="col">Proprietaire</th> --}}
                            <th scope="col">Immatriculation</th>
                            <th scope="col">Sous-traitant</th>
                            <th></th>
                            <th></th>
                            </tr>
                        </thead>
                        <tbody class="card-body">
                            @if ($machines->isNotEmpty())
                            @foreach ($machines as $machine)
                            <tr class="text-center">
                                <td>{{$machine->type_machine}}</td>
                                {{-- <td>{{$machine->marque_machine}}</td>
                                <td>{{$machine->proprietaire_machine}}</td> --}}
                                <td>{{$machine->immatriculation_machine}}</td>
                            <td>
                                    @if($machine->sous_traitant_machine !== 0)
                                    Oui
                                    @else
                                    non
                                    @endif
                            </td>
                            <td><a href="{{route('machines.edit', $machine->id)}}" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="Modifier"><i class="fas fa-edit"></i></a></td>
                            <td><a href="{{route('machines.show', $machine->id)}}" class="btn btn-danger"><i class="fas fa-trash"></i></a></td>
                            </tr>
                            @endforeach
                            @else()
                                <h3 class="card-header text-center">Vous n'avez pas de machine créé</h3>
                            @endif
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection