<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewSiteInvitation extends Model
{
    protected $fillable = [
        'email_invitation',
        'objet_invitation',
        'message_invitation'
    ];
}
