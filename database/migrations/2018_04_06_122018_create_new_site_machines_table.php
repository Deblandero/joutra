<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewSiteMachinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_site_machines', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->text('type_machine')->nullable();
            $table->text('marque_machine')->nullable();
            $table->text('proprietaire_machine')->nullable();
            $table->text('immatriculation_machine');
            $table->boolean('sous_traitant_machine')->default(0)->nullable();
            $table->integer('user_id');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_site_machines');
    }
}
