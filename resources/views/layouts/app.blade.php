<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    <head>

    @include('partials._head')
    
    </head>
    <body>
    <header>
        @include('partials._nav_auth')
    </header>

    <main>
        @yield('content')
    </main>

    <footer>
        @include('partials._footer')
    </footer>
    </body>
</html>
