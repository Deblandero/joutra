@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection

{{-- Data's autogénérés --}}
@php

//date
$today = date("m.d.y");

//météo

@endphp
{{-- END Data's autogénérés --}}

@section('content')
        {!! Form::open(['route' => ['pdf-fiche-journaliere.store']]) !!}
        {!! Form::hidden('id_chantier', $sites->id)!!}
<div class="container">
    {{-- titre chantier --}}
    <div class="row">
        <div class="col-md-12 text-center title-journa">
            <h4 class="">{{$sites->nom_chantier}}</h4>
            <a href="{{route('principale')}}" type="button" class="btn btn-secondary"><i class="fas fa-home"></i></a>
        </div>
    </div>  
    {{-- information journalière --}}
    <div class="row">
            <div class="col-md-4 text-center info-journa">
                <ilayer class="meteo-journa">
                    <iframe src="http://www.meteobelgium.be/service/fr/code/index.php?code={{$sites->code_postal}}&type=ville"
                    allowtransparency="true" align="center" frameborder="0" width="170" height="125"
                    scrolling="no" marginwidth="0" marginheight="0">
                    <a href="http://www.meteobelgique.be" target="_new">www.meteobelgique.be</a>
                    </iframe>
                </ilayer>
            </div>

            <div class="col-md-4 text-center info-journa">
                <div class="date-journa">
                {!!Form::date('date', \Carbon\Carbon::now(), ['class'=>'form-control form-control-lg']) !!}
                </div>
            </div>

            <div class="col-md-4 text-center info-journa">
                <h6 class="coordonne-joutra">{{$sites->rue_chantier}}, {{$sites->num_rue}}</h6>
                <h6 class="coordonne-joutra">à {{$sites->ville}}</h6>
            </div>
    </div>
    <hr>
        {{-- actors --}}
    <div class="row">
        <div class="col-md-6 text-center actor-journa">
            <h5>Maitre d'ouvrage</h5>
            <p>{{$sites->maitre_ouvrage}}</p>
        </div>
        <div class="col-md-6 text-center actor-journa">
            <h5>Maitre d'oeuvre</h5>
            <p>{{$sites->maitre_oeuvre}}</p>
        </div>
    </div>
    <hr>
    {{-- ouvriers machines --}}
    <div class="row">
        <div class="col-md-6 text-center">
            <h5>Ouvriers   <a href="{{route('ouvriers.create')}}"><i class="fas fa-plus"></i></a></h5>
            @foreach( $bobs as $bob)
            <div class="form-row">
                <div class="col-4">
                    {{$bob->metier_ouvrier}}
                </div>
                <div class="col-2">
                    {!!Form::text($bob->metier_ouvrier, null,['class'=>'form-control'])!!}
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-md-6 text-center">
            <h5>Machines   <a href="{{route('machines.create')}}"><i class="fas fa-plus"></i></a></h5>
            @foreach( $machines as $machine)
            <div class="form-row">
                <div class="col-4">
                    {{$machine->type_machine}}
                </div>
                <div class="col-2">
                    {!!Form::text($machine->type_machine, null,['class'=>'form-control'])!!}
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <hr>
    {{-- travaux --}}
    <div class="row">
        <div class="col-md-12">
            <h5>Travaux exécutés:</h5>
            {!!Form::textarea('travaux', null, ['class'=>'textarea-journa'])!!}
        </div>
    </div>   
        {{--matériaux  --}}
    <div class="row">    
        <div class="col-md-12">
            <h5>Matériaux entré ce jour:</h5>
            {!!Form::textarea('materiaux', null, ['class'=>'textarea-journa'])!!}
        </div>
    </div>
    {{-- essais --}}
    <div class="row">
        <div class="col-md-12">
            <h5>Essais:</h5>
            {!!Form::textarea('essais', null, ['class'=>'textarea-journa'])!!}
        </div>
    </div>
    {{-- commentaire --}}
    <div class="row">    
        <div class="col-md-12">
            <h5>Commentaire:</h5>
            {!!Form::textarea('commentaire', null, ['class'=>'textarea-journa'])!!}
        </div>
    </div>
    {{-- buttons --}}
    <div class="row">
        <div class="col-md-12 btn-journa">
            <a href="{{route('principale')}}" type="button" class="btn btn-secondary btn-lg btn-block">Accueil</a>
            {!! Form::submit('Aperçu la fiche', ['class'=>'btn btn-secondary btn-lg btn-block'] ) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>            

@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

{{--  Calendrier Datepicker  --}}
    <script type="text/javascript">
        $(function() {
            $( "#datepicker" ).datepicker({
            // changeMonth: true,
            // changeYear: true
            });
        });
    </script>

@endsection