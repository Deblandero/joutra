<?php

namespace App\Http\Controllers;

use App\NewSiteInvitation;
use Illuminate\Http\Request;

class NewSiteInvitationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('new_site.invitation.invitation_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'email_invitation' => 'required|email',
            'objet_invitation' => 'required|string',
            'message_invitation' => 'required|string'
        ));

        $newsiteinvitation = new NewSiteInvitation;

        $newsiteinvitation->email_invitation = $request->email_invitation;
        $newsiteinvitation->objet_invitation = $request->objet_invitation;
        $newsiteinvitation->message_invitation = $request->message_invitation;

        $newsiteinvitation->save();

        return redirect()->route('invitations.show', $newsiteinvitation->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $newsiteinvitation = NewSiteInvitation::find($id);
        return view('new_site.test')->withPost($newsiteinvitation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
