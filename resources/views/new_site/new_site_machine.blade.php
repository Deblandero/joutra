@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection

@section('content')

<div class="container">

@include('partials.new_site._new_site_nav')

<section class="machine">
    <div class="row">
        <div class="col-md-6">
            <p>liste des machines enregistrée</p>
        </div>
        <div class="col-md-6">
            <form method="POST" action="{{ route('machines.store' )}}" data-parsley-validate="">
            @csrf
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="type_machine">Type</label>
                        <input type="text" class="form-control" id="type_machine" placeholder="Type" name="type_machine" required="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="marque_machine">Marque</label>
                        <input type="text" class="form-control" id="marque_machine" name="marque_machine" placeholder="Marque">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="proprietaire_machine">Propriétaire</label>
                        <input type="text" class="form-control" id="proprietaire_machine" name="proprietaire_machine" placeholder="Propriétaire" required="">
                    </div>
                    <div class="form-group col-md-9">
                        <label for="immatriculation_machine">Immatriculation</label>
                        <input type="text" class="form-control" id="immatriculation_machine" name="immatriculation_machine" placeholder="Immatriculation" required="">
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="sous_traitant_machine" name="sous_traitant_machine" value="1" name="sous_traitant_machiner">
                        <label class="custom-control-label" for="sous_traitant_machine">Sous-traitant</label>
                    </div>
                    <button type="submit" class="btn btn-outline-success">Ajouter</button>     
            </form>
        </div>
    </div>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection