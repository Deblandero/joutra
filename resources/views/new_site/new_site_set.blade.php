@extends('layouts.app')

{{--  Validation client, Date  --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui-theme.css') }}" rel="stylesheet">

@endsection


@section('content')

<div class="container">

@include('partials.new_site._new_site_nav')

<section class="parametre">
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{ route('parametre.store' )}}" data-parsley-validate="">
            @csrf

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="nom_chantier">Nom du chantier</label>
                        <input type="text" name="nom_chantier" class="form-control" required="" id="nom_chantier" placeholder="Nom du chantier">                      
                    </div>
                    <div class="form-group col-md-6">
                        <label for="rue_chantier">Rue</label>
                        <input type="text" name="rue_chantier" class="form-control" required="" id="rue" placeholder="rue">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="num_csc">Numéro du cahier spécial des charges</label>
                        <input type="text" name="num_csc" class="form-control" required="" id="num_csc" placeholder="csc">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="num_rue">Numéro</label>
                        <input type="text" name="num_rue" class="form-control" id="num_rue" placeholder="Numéro">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="code_postal">Code Postal</label>
                        <input type="text" name="code_postal" class="form-control" id="code_postal" placeholder="Code Postal">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="num_dossier">Numéro du dossier</label>
                        <input type="text" name="num_dossier" class="form-control" required="" id="num_dossier" placeholder="Dossier">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="ville">Ville</label>
                        <input type="text" name="ville" class="form-control" id="ville" placeholder="Ville">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="date_debut">Date de début</label>
                        <input type="date" name="date_debut" class="form-control" required="" id="date_debut">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="date_fin">Date de fin</label>
                        <input type="date" name="date_fin" class="form-control" required="" id="date_fin">
                    </div>    
                    <div class="form-group col-md-6">
                        <label for="bonus">...</label>
                        <input type="text" name="..." class="form-control" id="bonus" placeholder="" disabled>
                    </div>
                    <button type="submit" class="btn btn-outline-primary">Enregistrer</button>   
            </form>
        </div>
    </div>

{{--  Calendrier Datepicker  --}}
    <script type="text/javascript">
        $(function() {
            $( "#datepicker" ).datepicker({
            // changeMonth: true,
            // changeYear: true
            });
        });
    </script>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

<script
			  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
			  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
			  crossorigin="anonymous"></script>

@endsection
