@section('title', ' | Nouveau chantier')


<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand {{ Request::is('nouveau-chantier*') ? "active" : " "}}" href="{{route('principale')}}">Accueil</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsite" aria-controls="navbarsite" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsite">
    <ul class="nav navbar-nav nav-justified nav-new-chantier">
    <li class="nav-item {{ Request::is('nouveau-chantier/parametre*') ? "active" : " "}}">
        <a class="nav-link" href="{{route('parametre.index')}}" data-toggle="tooltip" data-placement="bottom" title="Chantier"><i class="fas fa-road"></i> </a>
    </li>
    <li class="nav-item {{ Request::is('nouveau-chantier/ouvriers*') ? "active" : " "}}">
        <a class="nav-link" href="{{route('ouvriers.index')}}"  data-toggle="tooltip" data-placement="bottom" title="Ouvrier"><i class="fas fa-walking"></i> </i> </a>
    </li>
    <li class="nav-item {{ Request::is('nouveau-chantier/machines*') ? "active" : " "}}">
        <a class="nav-link" href="{{route('machines.index')}}"  data-toggle="tooltip" data-placement="bottom" title="Machine"><i class="fas fa-truck"></i></a>
    </li>
    <li class="nav-item">
        <a class="nav-link disabled" href="{{route('invitations.create')}}" data-toggle="tooltip" data-placement="bottom" title="Envoyer un courrier"><i class="far fa-envelope"></i></a>
    </li>
    </ul> 
   
  </div>
</nav>
