<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NewSiteSet;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.home');
    }

    //*********************************** */
    //    NOUVEAU CHANTIER                //   
    //  Controle de test pour les routes  //
    //*********************************** */

    public function new_site(){
        //return view('new_site.new_site');
    
    
        $sets = NewSiteSet::where('user_id', Auth::user()->id)->get();
    
        return view('pages.principale')->withSets($sets);
    }

    //parametre
    public function new_site_set(){
        return view('new_site.new_site_set');
    }

    public function new_site_post_set(){
        return view('new_site.new_site_set');
    }

    //humain
    public function new_site_bob(){
        return view('new_site.new_site_bob');
    }

    public function new_site_post_bob(){
        return view('new_site.new_site_bob');
    }

    //machine
    public function new_site_machine(){
        return view('new_site.new_site_machine');
    }

    public function new_site_post_machine(){
        return view('new_site.new_site_machine');
    }

    //invitation
    public function new_site_invitation(){
        return view('new_site.new_site_invitation');
    }

    public function new_site_post_invitation(){
        return view('new_site.new_site_invitation');
    }
}
