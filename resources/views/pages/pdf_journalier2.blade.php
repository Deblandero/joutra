<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Joutra @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <style>

@page {
            margin-top: 0.3em;
            margin-left: 0.6em;
        }

.page {
    background-color: white;

    margin: 0;
}

.page-title {
    background-color: #c7daea;
    height: 20mm;
    width: 200mm;
    border: 1px solid #000;
    text-align: center;
    position: absolute;
    top: 1mm;
    margin: 3mm;
}

.parametre {
    height: 60mm;
    width: 200mm;
}

.param1{
    top: 27mm;
    left: 10mm;
    position: absolute;
}

.param2{
    top: 27mm;
    left: 80mm;
    position: absolute;
}

.param3{
    top: 27mm;
    left: 140mm;
    position: absolute;
}

.param4{
    top: 65mm;
    left: 3mm;
    height: 34mm;
    width: 94mm;
    position: absolute;
    border: 1px solid #000;
    padding: 3mm;
}

.param5{
    top: 65mm;
    left: 103mm;
    height: 34mm;
    width: 94mm;
    position: absolute;
    border: 1px solid #000;
    padding: 3mm;
}

.travaux{
    top: 105mm;
    left: 3mm;
    width: 94mm;
    height: 39mm;
    position: absolute;
    border: 1px solid #000;
    padding: 3mm;
}

.materiaux{
    top: 105mm;
    left: 103mm;
    width: 94mm;
    height: 69mm;
    position: absolute;
    border: 1px solid #000;
    padding: 3mm;
}

.commentaire{
    top: 150mm;
    left: 3mm;
    width: 94mm;
    height: 84mm;
    position: absolute;
    border: 1px solid #000;
    padding: 3mm;
}

.essais{
    top: 180mm;
    left: 103mm;
    width: 94mm;
    height: 54mm;
    position: absolute;
    border: 1px solid #000;
    padding: 3mm;
}

.signature{
    top: 245mm;
    left: 10mm;
    width: 100mm;
    height: 25mm;
    position: absolute;

}
</style>


</head>
    <body>

    <main>
            <div class="page">
                <div class="page-title">
                    <h3 class="">Journal de travaux</h3>
                </div>
                {{-- parametres du chantier --}}
                <div class="parametre">
                    <div class="param1">
                        <strong>Maître de l'ouvrage:</strong>
                        <p>{{$site->maitre_ouvrage}}</p>

                        <strong>Maître d'oeuvre:</strong>
                        <p>{{$site->maitre_oeuvre}}</p>
                    </div>
                    <div class="param2">
                        <address>
                            <strong>{{$site->nom_chantier}}</strong>
                            <br>
                            {{$site->rue_chantier}}, {{$site->num_rue}}
                            <br>
                            {{$site->code_postal}}, {{$site->ville}}
                        </address>
                    </div>
                    <div class="param3">
                        @if($date == date('Y-m-d') )
                        <ilayer>
                            <iframe class="" src="http://www.meteobelgium.be/service/fr/code/index.php?code={{$site->code_postal}}&type=ville"
                            allowtransparency="true" align="center" frameborder="0" width="170" height="125"
                            scrolling="no" marginwidth="0" marginheight="0">
                            <a href="http://www.meteobelgique.be" target="_new">www.meteobelgique.be</a>
                            </iframe>
                        </ilayer>
                        @else
                            <p class="">Date: </p>
                        @endif
                    </div>
                </div>
                
                {{-- Ouvriers et Machines --}}
                <div class="ouvriermachine">
                    <div class="param4">
                        <strong>Ouvriers présents:</strong>
                        <p>@foreach($ouvriers as $ouvrier)
                            {{$ouvrier}}
                        @endforeach</p>
                    </div>
                    <div class="param5">
                        <strong>Matériels présents:</strong>
                        <p>@foreach($materiels as $materiel)
                            {{$materiel}}
                        @endforeach</p>
                    </div>
                </div>

                {{-- travaux --}}
                <div class="travaux">
                    <strong>Travaux éxécuté:</strong>
                    <p>{{$travaux}}</p>
                </div>

                {{-- materiaux --}}
                <div class="materiaux">
                    <strong>Matériaux entré ce jour:</strong>
                    <p>{{$materiaux}}</p>
                </div>

                {{-- commentaire --}}
                <div class="commentaire">
                    <strong>Commentaire:</strong>
                    <p>{{$commentaire}}</p>
                </div>

                {{-- essais --}}
                <div class="essais">
                    <strong>Essais:</strong>
                    <p>{{$essais}}</p>
                </div>

                {{-- signature --}}
                <div class="signature">
                    <p><strong>Signature:</strong></p>
                        
                </div>

            </div>
    </main>
    </body>
</html>