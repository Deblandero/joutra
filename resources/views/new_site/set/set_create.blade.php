@extends('layouts.app')

{{--  Validation client, Date  --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui-theme.css') }}" rel="stylesheet">

@endsection


@section('content')

<div class="container-fluid"> 
@include('partials.new_site._new_site_nav')
<section class="">
    <div class="row">
        <div class="col-md-12">
            <div class="card content">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="card-header">
                    <h5 class="text-center">Ajouter des paramètres a votre chantier</h5>
                </div>
                
                <form method="POST" action="{{ route('parametre.store' )}}" data-parsley-validate="">
                @csrf
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="maitre_oeuvre" class="col-md-2 col-form-label">Maître d'oeuvre</label>
                        <div class="col-md-4">
                        <input type="text" name="maitre_oeuvre" class="form-control" required="" id="maitre_oeuvre" placeholder="Obligatoire">                      
                        </div>
                        <label for="maitre_ouvrage" class="col-md-2 col-form-label">Maître d'ouvrage</label>
                        <div class="col-md-4">
                        <input type="text" name="maitre_ouvrage" class="form-control" required="" id="maitre_ouvrage" placeholder="Obligatoire">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nom_chantier" class="col-md-2 col-form-label">Nom du chantier</label>
                        <div class="col-md-4">
                        <input type="text" name="nom_chantier" class="form-control" required="" id="nom_chantier" placeholder="Obligatoire">                      
                        </div>
                        <label for="num_csc" class="col-md-2 col-form-label">CSC</label>
                        <div class="col-md-4">
                        <input type="text" name="num_csc" class="form-control" required="" id="num_csc" placeholder="Obligatoire">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="rue_chantier" class="col-md-2 col-form-label">Rue</label>
                        <div class="col-md-4">
                        <input type="text" name="rue_chantier" class="form-control" required="" id="rue" placeholder="Obligatoire">
                        </div>
                        <label for="num_rue" class="col-md-2 col-form-label">Numéro</label>
                        <div class="col-md-4">    
                        <input type="text" name="num_rue" class="form-control" id="num_rue">
                        </div>
                    </div>    
                    <div class="form-group row">
                        <label for="code_postal" class="col-md-2 col-form-label">Code Postal</label>
                        <div class="col-md-4">
                        <input type="text" name="code_postal" class="form-control" id="code_postal" required="" placeholder="Obligatoire">
                        </div>
                        <label for="ville" class="col-md-2 col-form-label">Ville</label>
                        <div class="col-md-4">
                        <input type="text" name="ville" class="form-control" id="ville">
                        </div>
                    </div>
                    <div class="form-group row">    
                        <label for="date_debut" class="col-md-2 col-form-label">Date de début</label>
                        <div class="col-md-4">
                        <input type="date" name="date_debut" class="form-control" required="" id="date_debut">
                        </div>
                        <label for="date_fin" class="col-md-2 col-form-label">Date de fin</label>
                        <div class="col-md-4">
                        <input type="date" name="date_fin" class="form-control" required="" id="date_fin"> 
                        </div>
                    </div>
                </div>    
                <div class="card-footer">
                    <div class="form-group row">
                        <button type="submit" class="col-md-4 offset-md-4 btn btn-success btn-block btn-sub">Enregistrer</button>    
                    </div>
                </div>
                </form>  
            </div>
        </div>
    </div>

{{--  Calendrier Datepicker  --}}
    <script type="text/javascript">
        $(function() {
            $( "#datepicker" ).datepicker({
            // changeMonth: true,
            // changeYear: true
            });
        });
    </script>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

<script
			  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
			  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
			  crossorigin="anonymous"></script>

@endsection
