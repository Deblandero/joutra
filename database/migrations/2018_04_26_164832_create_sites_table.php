<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('new_site_sets_id')->nullable();
            $table->foreign('new_site_sets_id')->references('id')->on('new_site_sets')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('new_site_bobs_id')->nullable();
            $table->foreign('new_site_bobs_id')->references('id')->on('new_site_bobs')->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('new_site_machines_id')->nullable();
            $table->foreign('new_site_machines_id')->references('id')->on('new_site_machines')->onDelete('cascade')->onUpdate('cascade');

            $table->text('metier_ouvrier')->nullable();
            $table->text('type_machine')->nullable();

        });
    }

    /**
     * w
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
