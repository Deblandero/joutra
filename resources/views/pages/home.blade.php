@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body text-center">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>Féliciation, vous êtes enregistré</p>
                    <a href="{{ route('principale') }}">Retour à Joutra</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
