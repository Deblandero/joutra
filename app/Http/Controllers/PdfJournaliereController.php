<?php

namespace App\Http\Controllers;

use App\Sites;
use App\NewSiteSet;
use App\NewSiteBob;
use App\NewSiteMachine;
use PDF;
use Illuminate\Http\Request;

class PdfJournaliereController extends Controller
{

    /**
     * Show the form for creating a new resource.
     * Impression de la fiche en PDF
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo "salut bro!";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $ouvriers = [];
        $materiels = [];

        // Le nom de la request a récupérer est $request->nom_du_champ_de_la_table
        // pour ça je boucle sur ma table avec $bobs, je récupère la valeur du champ que je recherche pour le comparer avec ma request

        $bobs = NewSiteBob::all();
        $machines = NewSiteMachine::all();
        $site = NewSiteSet::find($request->id_chantier);
        $bob = NewSiteBob::all();
        $machine = NewSiteMachine::all();
        

        //Listing des ouvriers
        foreach ($bobs as $bob) {
    
        $ouvrier = $bob->metier_ouvrier;
        
            foreach ($request->request as $key => $value) {
                if ($value != null) {
                    $key = str_replace("_", " ", $key);
                    if ($key == $ouvrier) {
                        $ouvriers []= $bob->metier_ouvrier.': '.$value;
                    }
                }
            }
        }

        
        //Listing des machines 
        foreach ($machines as $machine) {
            
            $materiel = $machine->type_machine;

            //Pour chaque key du request, je la reformate en retirant le "_" placé automatiquement via le faite qu'il est un nom de champ dans le formulaire.
            //une fois cela fait, je fais matcher le nom du champ avec le nom de la machine dans la DB et je stoque la valeur du form + le nom de la machine dans un tablbeau
            //Pour ensuite la retourner dans ma vue.

            foreach ($request->request as $key => $value) {
                if ($value != null) {
                    $key = str_replace("_", " ", $key);
                    if ($key == $materiel) {
                        $materiels []= $machine->type_machine.': '.$value;
                    }
                }
            }
        }

        //recup de la date 2018-mo-jo

        $date = $request->date;
        //recup des travaux executes
        if (isset($request->travaux)) {
                $travaux = $request->travaux;
            }else{
                $travaux = "Pas de travaux réalisé";
            }


        //recu des materiaux
        if (isset($request->materiaux)) {
                $materiaux = $request->materiaux;
            }else{
                $materiaux = "Pas de materiaux entré";
            }

        //recu des essais
        if (isset($request->essais)) {
                $essais = $request->essais;
            }else{
                $essais = "Pas d'essais exécuté";
            }

        //recu des commentaire
        if (isset($request->commentaire)) {
                $commentaire = $request->commentaire;
            }else{
                $commentaire = "Pas de commentaire";
            }



        //return view('pages.pdf_journalier2',compact('ouvriers','materiels','site','bob','machine','travaux','materiaux','essais','commentaire','date'));    

        $data = compact('ouvriers','materiels','site','bob','machine','travaux','materiaux','essais','commentaire','date');

        $pdf = PDF::loadView('pages.pdf_journalier2', $data)->setPaper('a4', 'portrait');
        return $pdf->download('pdf-journalier'.$date.'.pdf');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('pages.pdf_journalier');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $site = NewSiteSet::find($id);
        $bob = NewSiteBob::all();
        $machine = NewSiteMachine::all();

        return view('pages.pdf_journalier',compact('site','bob','machine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }



}
