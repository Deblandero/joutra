@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection



@section('content')


<div class="container-fluid">
@include('partials.new_site._new_site_nav')
<section class="main-center">
    <div class="row"> 
        <div class="col">
            <div class="card content">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-header">
                    <h5 class="text-center">Modifier votre ouvrier</h5>
                </div>
                
            {!! Form::model($bob, ['route' => ['ouvriers.update', $bob->id],'data-parsley-validate'=>'','method'=>'PUT']) !!}  
            <div class="card-body">
                {{Form::label('metier_ouvrier', 'Metier:')}}
                {!!Form::text('metier_ouvrier', null,['class'=>'form-control', 'required'=>'', 'placeholder'=>'Obligatoire'])!!}

                {{Form::label('employeur_ouvrier', 'Employeur:')}}
                {!!Form::text('employeur_ouvrier', null,['class'=>'form-control'])!!}

                {{Form::label('sous_traitant_ouvrier', 'Soutraitant:')}}
                {!!Form::checkbox('sous_traitant_ouvrier', '1',['class'=>'form-control'])!!}
                <hr>
                <a href="{{ route('ouvriers.index', $bob->id )}}" class="btn btn-primary">Annuler</a>
                {!! Form::submit('Sauvegarder les changements', ['class'=>'btn btn-success'] ) !!}
            </div>
            {{Form::close()}}
            </div>
        </div>    
    </div>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection