@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui-theme.css') }}" rel="stylesheet">


@endsection


@section('content')

<div class="container-fluid">
@include('partials.new_site._new_site_nav')
<section class="">
    <div class="row"> 
        <div class="col">
            <div class="card content">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-header">
                    <h5 class="text-center">Modifier les paramètres de votre chantier</h5>
                </div>
                {!! Form::model($set, ['route' => ['parametre.update', $set->id],'data-parsley-validate'=>'','method'=>'PUT']) !!}    
                <div class="card-body">
                    <div class="form-group row">
                    {{Form::label('maitre_oeuvre', 'Maître-oeuvre', ['class'=> 'col-md-2 col-form-label'])}}
                    {{Form::text('maitre_oeuvre', null, ['class'=>'form-control col-md-4', 'required'=>'', 'placeholder'=>'Obligatoire'])}}

                    {{Form::label('maitre_ouvrage', 'Maitre-ouvrage:', ['class'=> 'col-md-2 col-form-label'])}}
                    {{Form::text('maitre_ouvrage', null, ['class'=>'form-control col-md-4', 'required'=>'', 'placeholder'=>'Obligatoire'])}}
                    </div>
                    <div class="form-group row">
                    {{Form::label('nom_chantier', 'Nom du chantier:', ['class'=> 'col-md-2 col-form-label'])}}
                    {{Form::text('nom_chantier', null, ['class'=>'form-control col-md-4', 'required'=>'', 'placeholder'=>'Obligatoire'])}}

                    {{Form::label('num_csc', 'Numéro csc:', ['class' =>'col-md-2 col-form-label'])}}
                    {{Form::text('num_csc', null, ['class'=>'form-control col-md-4', 'required'=>'', 'placeholder'=>'Obligatoire'])}}
                    </div>
                    <div class="form-group row">
                    {{Form::label('rue_chantier', 'Rue du chantier:', ['class'=> 'col-md-2 col-form-label'])}}
                    {{Form::text('rue_chantier', null, ['class'=>'form-control col-md-4', 'required'=>'', 'placeholder'=>'Obligatoire'])}}

                    {{Form::label('num_rue', 'Numéro:', ['class'=> 'col-md-2 col-form-label'])}}
                    {{Form::text('num_rue', null, ['class'=>'form-control col-md-4'])}}
                    </div>
                    <div class="form-group row">
                    {{Form::label('code_postal', 'Code postal:', ['class'=> 'col-md-2 col-form-label'])}}
                    {{Form::number('code_postal', null, ['class'=>'form-control col-md-4'])}}

                    {{Form::label('ville', 'Ville:', ['class'=> 'col-md-2 col-form-label'])}}
                    {{Form::text('ville', null, ['class'=>'form-control col-md-4'])}}
                    </div>
                    <div class="form-group row">
                    {{Form::label('date_debut', 'Date de début:', ['class'=> 'col-md-2 col-form-label'])}}
                    {{Form::date('date_debut', null, ['class'=>'form-control col-md-4', 'required'=>'', 'placeholder'=>'Obligatoire'])}}

                    {{Form::label('date_fin', 'Date de fin:', ['class'=> 'col-md-2 col-form-label'])}}
                    {{Form::date('date_fin', null, ['class'=>'form-control col-md-4', 'required'=>'', 'placeholder'=>'Obligatoire'])}}
                    </div>
                </div>
                <div class="card-footer">
                    <div class="form-group row">
                        <a href="{{ route('parametre.index', $set->id )}}" class="col-md-3 offset-md-2 btn btn-primary btn-sub">Annuler</a>
                        {!! Form::submit('Sauvegarder', ['class' => 'col-md-3 offset-md-2 btn btn-success btn-sub']) !!}
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection