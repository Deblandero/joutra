@extends('layouts.app')


@section('content')

<div class="container">

@include('partials.new_site._new_site_nav')

<section class="card invitation">

<h1>{{ $bob->nom_ouvrier }}</h1>

<h2>{{ $bob->prenom_ouvrier }}</h2>

<p>{{ $bob->metier_ouvrier }}</p>

<a class="btn-btn-primary" href="#">Modifier</a>

</section>
  
</div>
@endsection
