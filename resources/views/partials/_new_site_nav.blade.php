@section('title', ' | Nouveau chantier')

<section class="nav-new-chantier">
        <nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-between">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNewChantier" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNewChantier">
            <ul class="navbar-nav mr-auto">
            <li class="nav-item">
            <a class="nav-link active" href="{{route('parametre.create')}}"><h3>Paramètres</h3></a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="#"><h3>Ouvriers</h3></a>
            </li>
                <li class="nav-item">
                <a class="nav-link" href="#"><h3>Machines</h3></a>
            </li>
                <li class="nav-item">
                <a class="nav-link" href="#"><h3>Invitations</h3></a>
            </li>
            </ul>
        </div>
    <a class="btn btn-outline-success my-2 my-sm-0" href="{{route('principale')}}">Principale</a>
        </nav>
    </section>