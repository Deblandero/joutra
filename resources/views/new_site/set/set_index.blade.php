@extends('layouts.app')


@section('content')

<div class="container-fluid">
@include('partials.new_site._new_site_nav')
<section class="">
    <div class="row"> 
        <div class="col">
            <div class="card content">
                <div class="card-header">
                <h5 class="text-center">Liste de vos chantiers</h5>
                <a href="{{route('parametre.create')}}" class="btn btn-success btn-add">Ajouter un chantier</a>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                </div>
                <table class="table">
                    <thead class="card-title text-center">
                        <tr>
                        <th scope="col">Chantier</th>
                        <th scope="col"></th>
                        <th scope="col"></th>

                        </tr>
                    </thead>
                    <tbody class="card-body">
                        @if ($sets->isNotEmpty())
                        @foreach ($sets as $set)
                        <tr class="text-center">
                            <td>
                                {{$set->nom_chantier}}
                            </td>
                            <td>
                            <a href="{{route('parametre.edit', $set->id)}}" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="Modifier"><i class="fas fa-edit"></i></a>
                            </td>
                            <td><a href="{{route('parametre.show', $set->id)}}" class="btn btn-danger"><i class="fas fa-trash"></i></a></td>
                        </tr>    
                        @endforeach
                        @else()
                            <h3 class="card-header text-center">Vous n'avez pas de chantier</h3>
                        @endif    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
  
</div>
@endsection
