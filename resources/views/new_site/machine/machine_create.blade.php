@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection

@section('content')

<div class="container-fluid">
                    @include('partials.new_site._new_site_nav')
<section class="">
    <div class="row">
        <div class="col">
            <div class="card content">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-header">
                    <h5 class="text-center">Ajouter une nouvelle machine</h5>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('machines.store' )}}" data-parsley-validate="">
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                    @csrf
                    <div class="form-group row">
                        <label for="type_machine" class="col-sm-3 col-form-label">Type</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="type_machine" placeholder="Obligatoire" name="type_machine" required="">
                        </div>
                    </div>
                    {{-- <div class="form-group row">    
                        <label for="marque_machine" class="col-sm-3 col-form-label">Marque</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="marque_machine" name="marque_machine">
                        </div>
                    </div>
                    <div class="form-group row"> 
                        <label for="proprietaire_machine" class="col-sm-3 col-form-label">Propriétaire</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="proprietaire_machine" name="proprietaire_machine" placeholder="Obligatoire" required="">
                        </div>
                    </div> --}}
                    <div class="form-group row"> 
                        <label for="immatriculation_machine" class="col-sm-3 col-form-label">Immatriculation</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="immatriculation_machine" name="immatriculation_machine" placeholder="Obligatoire" required="">
                        </div>
                    </div>
                    <div class="custom-control custom-checkbox"> 
                        <input type="checkbox" class="custom-control-input" id="sous_traitant_machine" name="sous_traitant_machine" value="1" name="sous_traitant_machiner">
                        <label class="custom-control-label" for="sous_traitant_machine" >Sous-traitant</label>
                    </div>
                </div>    
                <div class="card-footer">
                    <div class="form-group row"> 
                        <button type="submit" class="col-sm-8 offset-sm-2 btn btn-success btn-block btn-sub">Ajouter</button>  
                    </div>    
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection