<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewSiteBob extends Model
{
    protected $fillable = [
        'nom_ouvrier',
        'prenom_ouvrier',
        'metier_ouvrier',
        'employeur_ouvrier',
        'sous_traitant_ouvrier',
        'user_i'
    ];
}
