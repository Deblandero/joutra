@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection

@section('content')

<div class="container">

@include('partials.new_site._new_site_nav')

<section class="invitation">
    <form method="POST" action="{{ route('invitations.store' )}}" data-parsley-validate="">
    @csrf
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="email_invitation">Email du destinataire</label>
                <input type="email" class="form-control" id="email_invitation" name="email_invitation" placeholder="Email" required="">
            </div>
            <div class="form-group col-md-12">
                <label for="objet_invitation">Objet</label>
                <input type="text" class="form-control" id="objet_invitation" name="objet_invitation" placeholder="Objet" required="">
            </div>
            <div class="form-group col-md-12">
                <label for="message_invitation">Message</label>
                <textarea type="text" class="form-control" id="message_invitation" name="message_invitation" placeholder="Message" rows="8" required=""></textarea>
            </div>
            <button type="submit" class="btn btn-outline-success">Envoyer</button>     
    </form>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection