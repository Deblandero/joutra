@extends('layouts.layout_journalier')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="page">
                <section class="page-content">
                    <div class="row"> 
                        <div class="col-sm-12 page-title text-center">
                            <h3 class="text-center">Journal de travaux</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 border border-dark entrepreneur">
                            <div class="row">
                                <blockquote class="col content-entrepreneur blockquote">
                                    <strong>Maître de l'ouvrage:</strong>
                                    <p>{{$site->maitre_ouvrage}}</p>
                                </blockquote>
                            </div>    
                            <div class="row">
                                <blockquote class="col content-entrepreneur blockquote">
                                    <strong>Maître d'oeuvre:</strong>
                                    <p>{{$site->maitre_oeuvre}}</p>
                                </blockquote>
                            </div>  
                        </div>
                        <div class="col-6 border border-dark parametre">
                            <div class="row">
                                <div class="col-6 text-left parametre-pdfjourna">
                                    <address>
                                    <strong>{{$site->nom_chantier}}</strong>
                                    <br>
                                    {{$site->rue_chantier}}, {{$site->num_rue}}
                                    <br>
                                    {{$site->code_postal}}, {{$site->ville}}
                                    </address>
                                </div>
                                <div class="col-6 text-right meteo-pdfjourna">
                                    @if($date == date('Y-m-d') )
                                    <ilayer>
                                        <iframe class="" src="http://www.meteobelgium.be/service/fr/code/index.php?code={{$site->code_postal}}&type=ville"
                                        allowtransparency="true" align="center" frameborder="0" width="170" height="125"
                                        scrolling="no" marginwidth="0" marginheight="0">
                                        <a href="http://www.meteobelgique.be" target="_new">www.meteobelgique.be</a>
                                        </iframe>
                                    </ilayer>
                                    @else
                                        <p class="text-left">Date: </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 border border-dark ouvrier">
                            <blockquote class="ouvrier-pdfjourna">
                                <strong>Ouvriers présents:</strong>
                                @foreach($ouvriers as $ouvrier)
                                    <p>{{$ouvrier}}</p>
                                @endforeach
                            </blockquote>
                        </div>
                        <div class="col-6 border border-dark travaux">
                            <blockquote class="travaux-pdfjourna">
                            <strong>Travaux éxécuté:</strong>
                                <p>{{$travaux}}</p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 border border-dark materiaux">
                            <blockquote class="materiaux-pdfjourna">
                            <strong>Matériaux entré ce jour:</strong>
                                <p>{{$materiaux}}</p>
                            </blockquote>
                        </div>
                        <div class="col-6 border border-dark machine">
                                <blockquote class="materiel-pdfjourna">
                                <strong>Matériels présents:</strong>
                                @foreach($materiels as $materiel)
                                    <p>{{$materiel}}</p>
                                @endforeach
                            </blockquote>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-6 border border-dark commentaire">
                            <blockquote class="commentaire-pdfjourna">
                            <strong>Commentaire:</strong>
                                <p>{{$commentaire}}</p>
                            </blockquote>
                        </div>
                        <div class="col-6 border border-dark essais">
                            <blockquote class="essais-pdfjourna">
                            <strong>Essais:</strong>
                                <p>{{$essais}}</p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 border border-dark signature">
                            <blockquote class="">
                            <strong>Signature:</strong>
                                
                            </blockquote>
                        </div>
                        <div class="col-6 border border-dark signature">
                            <blockquote class=""">
                            <strong>Taratata:</strong>
                                
                            </blockquote>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <section class="button-pdf">
                <a class="btn btn-primary btn-lg" data-toggle="tooltip" data-placement="bottom" title="Modifier" href="{{route('fiche-journaliere.edit', $site->id)}}" role="button"><i class="fa fa-edit"></i></a>
                <a href="{{route('pdf-fiche-journaliere.create')}}" class="btn btn-success btn-lg" data-toggle="tooltip" data-placement="bottom" title="Imprimer"><i class="fas fa-print"></i></a>
                <a href="#" class="btn btn-warning btn-lg" data-toggle="tooltip" data-placement="bottom" title="Liste des fiches"><i class="fas fa-list-ol"></i></a>

                <a href="{{route('principale')}}" class="btn btn-secondary btn-lg">Accueil</a>
            
            
            
            
            </section>
        </div>
    </div>
</div>

@endsection