@extends('layouts.app')

@section('title', ' | Principale')

@section('content')
@include('partials.new_site._new_site_nav')
<div class="container-fluid">

    <section class="principale">      
    @if ($sets->isNotEmpty())  
    @foreach ($sets as $set)       
        <div class="row">  
            <div class="col-md-12">
                <div class="jumbotron jumb-principale">
                    <h1 class="display-12">{{$set->nom_chantier}}</h1>
                    <p><strong>Maître d'ouvrage: </strong>{{$set->maitre_ouvrage}}</p>
                    <p><strong>Mâitre d'oeuvre: </strong>{{$set->maitre_oeuvre}}</p>
                    <hr class="my-12">
                    <p><strong>Début le: </strong>{{ date('d-m-y', strtotime($set->date_debut)) }}</p>
                    <p><strong>Fin le: </strong>{{ date('d-m-y', strtotime($set->date_fin))}}</p>
                    <p class="lead">
                        <a class="btn btn-primary btn-lg" href="{{route('fiche-journaliere.edit', $set->id)}}" role="button">Journalier</a>
                    </p>
                </div>
            </div>
        </div>    
        @endforeach
    @else
        <div class="col text-center">
            <div class="card">
                <div class="card-header">
                    <h2>Pas de chantier en cours</h2>
                    <a class="btn btn-outline-success text-center" href="{{ route('parametre.create') }}">Ajouter un chantier</a>
                </div>
            </div>
        </div>

    </section>    
    @endif 


</div>
@endsection
