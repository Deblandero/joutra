@extends('layouts.app')

@section('content')

<div class="container">

@include('partials.new_site._new_site_nav')

<section class="">

    <div class="jumbotron content">
        <h1 class="display-4">Paramètres de votre nouveau chantier</h1>
        <p class="lead">Veuillez remplir les différents feuillets, pour créer votre nouveau chantier</p>
        <hr class="my-4">
        <ul>
            <li>Chantiers: Les données principales</li>
            <li>Humains: Les moyens humains mis en place</li>
            <li>Machines: Les moyens mécaniques mis en place</li>
            <li>Invitation: Inviter d'autres acteurs en rapport avec votre chantier</li>
        </ul>
    </div>

</section>
  
</div>
@endsection