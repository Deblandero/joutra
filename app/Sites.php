<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sites extends Model
{
    protected $fillable = [
        'new_site_sets_id',
        'new_site_bobs_id',
        'new_site_machines_id',
        'metier_ouvrier',
        'type_machine',
    ];

    public function site(){
        return $this->hasOne('App\NewSiteSet');
    }

    public function bob(){
        return $this->belongsTo('App\NewSiteBob');
    }

    public function machine(){
        return $this->belongsTo('App\NewSiteMachine');
    }

}

