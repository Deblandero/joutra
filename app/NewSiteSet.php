<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewSiteSet extends Model
{
    protected $fillable = [
        'maitre_oeuvre',
        'maitre_ouvrage',
        'nom_chantier',
        'num_csc',
        'num_dossier',
        'date_debut',
        'date_fin',
        'rue_chantier',
        'num_rue',
        'code_postal',
        'ville',
        'user_id'
    ];

    
}