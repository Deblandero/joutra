@extends('layouts.app')

@section('content')

<div class="container">

@include('partials._new_site_nav')

<section class="bob">
    <div class="row">
        <div class="col-md-6">
            <p>liste des ouvriers enregistré</p>
        </div>
        <div class="col-md-6">
            <form method="POST" action="{{ route('new_site_post_bob' )}}">
            @csrf
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="nom_ouvrier">Nom *</label>
                        <input type="text" class="form-control" id="nom_ouvrier" placeholder="Nom">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="prenom_ouvrier">Prénom *</label>
                        <input type="text" class="form-control" id="prenom_ouvrier" placeholder="Prénom">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="metier_ouvrier">Métier *</label>
                        <input type="text" class="form-control" id="metier_ouvrier" placeholder="Métier">
                    </div>
                    <button type="submit" class="btn btn-outline-primary">Ajouter</button>     
            </form>
        </div>
    </div>
</section>
  
</div>
@endsection