<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewSiteSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_site_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->text('maitre_oeuvre');
            $table->text('maitre_ouvrage');
            $table->text('nom_chantier');
            $table->text('num_csc');
            $table->text('rue_chantier');
            $table->text('num_rue')->nullable();
            $table->integer('code_postal')->nullable();
            $table->text('ville')->nullable();
            $table->date('date_debut');
            $table->date('date_fin');
            $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_site_sets');
    }
}
