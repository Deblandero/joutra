<?php

namespace App\Http\Controllers;

use App\NewSiteSet;
use Illuminate\Http\Request;

class PrincipaleController extends Controller
{
    public function principale(){

        $sets = NewSiteSet::paginate(3);
    
        return view('pages.principale')->withSets($sets);

    }
}
