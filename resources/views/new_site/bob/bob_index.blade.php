@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection


@section('content')

<div class="container-fluid">
@include('partials.new_site._new_site_nav')
<section class="">
    <div class="row"> 
        <div class="col">
            <div class="card content">
                <div class="card-header">
                <h5 class="text-center">Liste de vos ouvrier</h5>
                <a href="{{route('ouvriers.create')}}" class="btn btn-success btn-add">Ajouter un ouvrier</a>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                </div> 
                <table class="table">
                    <thead class="card-title text-center">
                            <th scope="col">Metier</th>
                            <th scope="col">Employeur</th>
                            <th scope="col">Sous-traitant</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                   
                    <tbody>
                        @if ($bobs->isNotEmpty())
                        @foreach ($bobs as $bob)
                        <tr class="text-center">
                            <td>{{$bob->metier_ouvrier}}</td>
                            <td>{{$bob->employeur_ouvrier}}</td>
                        <td>
                                @if($bob->sous_traitant_ouvrier !== null)
                                Oui
                                @else
                                non
                                @endif
                        </td>
                        <td><a href="{{route('ouvriers.edit', $bob->id)}}" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="Modifier"><i class="fas fa-edit"></i></a></td>
                        <td><a href="{{route('ouvriers.show', $bob->id)}}" class="btn btn-danger"><i class="fas fa-trash"></i></a></td>
                        </tr>
                        @endforeach
                        @else()
                            <h3 class="card-header text-center">Vous n'avez pas d'ouvrier</h3>
                        @endif    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection