<?php

namespace App\Http\Controllers;

use App\NewSiteSet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;


class NewSiteSetController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sets = NewSiteSet::where('user_id', Auth::user()->id)->get();
        if ($sets) {
            return view('new_site.set.set_index')->withSets($sets);
        }
        else {
            return view('new_site.set.set_index');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('new_site.set.set_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'maitre_oeuvre' => 'required|string',
            'maitre_ouvrage' => 'required|string',
            'nom_chantier' => 'required|string',
            'num_csc' => 'required|string',
            'rue_chantier' => 'required|string',
            'num_rue' => 'nullable|string',
            'code_postal' => 'nullable|string',
            'ville' => 'nullable|string',
            'date_debut' => 'required|date',
            'date_fin' => 'required|date|after_or_equal:date_debut', 
        ));

        $newsiteset = new NewSiteSet();
     
        $newsiteset->maitre_oeuvre = $request->maitre_oeuvre;
        $newsiteset->maitre_ouvrage = $request->maitre_ouvrage;
        $newsiteset->nom_chantier = $request->nom_chantier;
        $newsiteset->num_csc = $request->num_csc;
        $newsiteset->rue_chantier = $request->rue_chantier;
        $newsiteset->num_rue = $request->num_rue;
        $newsiteset->code_postal = $request->code_postal;
        $newsiteset->ville = $request->ville;
        $newsiteset->date_debut = $request->date_debut;
        $newsiteset->date_fin = $request->date_fin;
        $newsiteset->user_id = $request->user_id;

        $newsiteset->save();

        return redirect()->route('parametre.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $set = NewSiteSet::find($id);
        return view('new_site.set.set_show')->withSet($set);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $set = NewSiteSet::find($id);
        return view('new_site.set.set_edit')->withSet($set);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'maitre_oeuvre' => 'required|string',
            'maitre_ouvrage' => 'required|string',
            'nom_chantier' => 'required|string',
            'num_csc' => 'required|string',
            'rue_chantier' => 'required|string',
            'num_rue' => 'nullable|string',
            'code_postal' => 'nullable|string',
            'ville' => 'nullable|string',
            'date_debut' => 'required|date',
            'date_fin' => 'required|date' 
        ));

        $set = NewSiteSet::find($id);

        $set->maitre_oeuvre = $request->maitre_oeuvre;
        $set->maitre_ouvrage = $request->maitre_ouvrage;
        $set->nom_chantier = $request->nom_chantier;
        $set->num_csc = $request->num_csc;
        $set->rue_chantier = $request->rue_chantier;
        $set->num_rue = $request->num_rue;
        $set->code_postal = $request->code_postal;
        $set->ville = $request->ville;
        $set->date_debut = $request->date_debut;
        $set->date_fin = $request->date_fin;
        $set->save();

        Session::flash('success', 'Vous avez bien modifié vos paramètres du chantier');

        return redirect()->route('parametre.show', $set->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $set = NewSiteSet::find($id);

        $set->delete();

        Session::flash('success', 'Votre chantier a bien été supprimé');

        return redirect()->route('parametre.index');
    }
}
