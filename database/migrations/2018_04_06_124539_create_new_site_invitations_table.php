<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewSiteInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_site_invitations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('email_invitation');
            $table->text('objet_invitation');
            $table->text('message_invitation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_site_invitations');
    }
}
