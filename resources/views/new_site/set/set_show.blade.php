@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection


@section('content')

<div class="container-fluid">
@include('partials.new_site._new_site_nav')
<section class="space-content">
    <div class="row"> 
        <div class="col">
            <div class="card content">
                <div class="card-header">
                    <h5 class="text-center">Informations sur votre chantier</h5>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                </div>
                <div class="card-body">
                    <h5 class="card-title"><strong>Maître d'oeuvre: </strong>{{ $set->maitre_oeuvre }}</h5>
                    <h5 class="card-title"><strong>Maître d'ouvrage: </strong>{{ $set->maitre_ouvrage }}</h5>
                    <h5 class="card-title"><strong>Nom: </strong>{{ $set->nom_chantier }}</h5>
                    <h5 class="card-title"><strong>Csc: </strong>{{ $set->num_csc }}</h5>
                    <h5 class="card-title"><strong>Rue: </strong>{{ $set->rue_chantier }}</h5>
                    <h5 class="card-title"><strong>Numéro: </strong>{{ $set->num_rue }}</h5>
                    <h5 class="card-title"><strong>Code postal: </strong>{{ $set->code_postal }}</h5>
                    <h5 class="card-title"><strong>Ville: </strong>{{ $set->ville }}</h5>
                    <h5 class="card-title"><strong>Début le: </strong>{{ date('d-m-y', strtotime($set->date_debut)) }}</h5>
                    <h5 class="card-title"><strong>Fin le: </strong>{{ date('d-m-y', strtotime($set->date_fin))}}</h5>
                </div>
                <div class="card-footer text-center">  
                    {{-- Buttons action --}}
                    {{Form::open(['route'=> ['parametre.destroy', $set->id], 'method'=>'DELETE'])}}
                    {{Form::button('<i class="far fa-trash-alt"></i>',['type'=>'submit','class'=>'btn btn-danger'])}}
                    <a href="{{ route('parametre.create' )}}" class="btn btn-success"><i class="far fa-plus-square"></i></a>
                    <a href="{{route('parametre.index')}}" class="btn btn-warning"><i class="fas fa-list-ol"></i></a>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</section>

</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection