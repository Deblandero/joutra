<?php

namespace App\Http\Controllers;

use App\NewSiteBob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;

class NewSiteBobController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bobs = NewSiteBob::where('user_id', Auth::user()->id)->get();
        return view('new_site.bob.bob_index')->withBobs($bobs);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('new_site.bob.bob_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,array(
        'nom_ouvrier' => 'nullable|string',
        'prenom_ouvrier' => 'nullable|string',
        'metier_ouvrier' => 'required|string',
        'employeur_ouvrier' => 'nullable|string',
        ));

        $newsitebob = new NewSiteBob;

        
        $newsitebob->nom_ouvrier = $request->nom_ouvrier;
        $newsitebob->prenom_ouvrier = $request->prenom_ouvrier;
        $newsitebob->metier_ouvrier = $request->metier_ouvrier;
        $newsitebob->employeur_ouvrier = $request->employeur_ouvrier;
        $newsitebob->sous_traitant_ouvrier = $request->sous_traitant_ouvrier;
        $newsitebob->user_id = $request->user_id;

        $newsitebob->save();


        return redirect()->route('ouvriers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bob = NewSiteBob::find($id);
        return view('new_site.bob.bob_show')->withBob($bob);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bob = NewSiteBob::find($id);
        return view('new_site.bob.bob_edit')->withBob($bob);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
        'nom_ouvrier' => 'nullable|string',
        'prenom_ouvrier' => 'nullable|string',
        'metier_ouvrier' => 'required|string',
        'employeur_ouvrier' => 'nullable|string',
        'sous_traitant_ouvrier' => 'nullable'
        
        ));

        $bob = NewSiteBob::find($id);

        $bob->nom_ouvrier = $request->input('nom_ouvrier');
        $bob->prenom_ouvrier = $request->input('prenom_ouvrier');
        $bob->metier_ouvrier = $request->input('metier_ouvrier');
        $bob->employeur_ouvrier = $request->input('employeur_ouvrier');
        $bob->sous_traitant_ouvrier = $request->input('sous_traitant_ouvrier');
        
        $bob->save();

        Session::flash('success', 'Vous avez bien modifié votre ouvrier');

        return redirect()->route('ouvriers.show', $bob->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bob = NewSiteBob::find($id);

        $bob->delete();

        Session::flash('success', 'Votre ouvrier a bien été supprimé');

        return redirect()->route('ouvriers.index');
    }
}
