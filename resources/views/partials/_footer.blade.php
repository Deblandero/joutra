<div class="container">
    <section class="footer">
        <div class="row">
            <div class="col">
                <p class="deblandero">Joutra by Deblandero | Tout droits réservé</p>
            </div>
        </div>
    </section>
</div>
 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        {{-- <script src="{{ asset('js/jquery-ui.js') }}"></script> --}}
    <script src="{{ asset('js/jquery.js') }}"></script>

    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    @yield('scripts')
