@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection


@section('content')

<div class="container-fluid">
@include('partials.new_site._new_site_nav')
<section class="">
    <div class="row"> 
        <div class="col">
            <div class="card content">
                <div class="card-header">
                    <h5 class="text-center">Informations sur votre machine</h5>
                </div>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            <div class="card-body">
                <h5 class="card-title"><strong>Type de machine: </strong>{{ $machine->type_machine }}</h5>
                {{-- <h5 class="card-title"><strong>Marque: </strong>{{ $machine->marque_machine }}</h5>
                <h5 class="card-title"><strong>Proprietaire: </strong>{{ $machine->proprietaire_machine }}</h5> --}}
                <h5 class="card-title"><strong>Immatriculation: </strong>{{ $machine->immatriculation_machine }}</h5>
                @if($machine->sous_traitant_machine)
                <h5 class="card-title">Sous-traitant</h5>
                @else
                <h5 class="card-title">Pas sous-traitant</h5>
                @endif
            </div>
            <div class="card-footer text-center">
                {{-- Buttons action --}}
                {{Form::open(['route'=> ['machines.destroy', $machine->id], 'method'=>'DELETE'])}}
                {{Form::button('<i class="far fa-trash-alt"></i>',['type'=>'submit','class'=>'btn btn-danger'])}}
                <a href="{{ route('machines.create' )}}" class="btn btn-success"><i class="far fa-plus-square"></i></a>
                <a href="{{route('machines.index')}}" class="btn btn-warning"><i class="fas fa-list-ol"></i></a>
                {{Form::close()}}
            </div>
            </div>
        </div>
    </div>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection