<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewSiteMachine extends Model
{
    protected $fillable = [
        'type_machine',
        'marque_machine',
        'proprietaire_machine',
        'immatriculation_machine',
        'sous_traitant_machine',
        'user_id'
    ];
}
