@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection

@section('content')

<div class="container-fluid">

@include('partials.new_site._new_site_nav')
<section class="">
    <div class="row">
        <div class="col">
           <div class="card content">
                <div class="card-header">
                    <h5 class="text-center">Inviter un autre utilisateur</h5>
                </div>                
                    <form method="POST" action="{{ route('invitations.store' )}}" data-parsley-validate="">
                    @csrf

                    <div class="card-body">

                        <div class="form-group row">
                            <label for="email_invitation" class="col-sm-4 col-form-label">Email du destinataire</label>
                            <div class="col-sm-8">
                            <input type="email" class="form-control" id="email_invitation" name="email_invitation" placeholder="Obligatoire" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="objet_invitation" class="col-sm-2 col-form-label">Objet</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="objet_invitation" name="objet_invitation" placeholder="Obligatoire" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="message_invitation" class="col-sm-2 col-form-label">Message</label>
                            <div class="col-sm-10">
                            <textarea type="text" class="form-control" id="message_invitation" name="message_invitation" placeholder="Obligatoire" rows="8" required=""></textarea>
                            </div>
                        </div>    
                </div>
                <div class="card-footer">
                    <div class="form-group row">
                    <button type="submit" class="col-sm-8 offset-sm-2 btn btn-success btn-block btn-sub">Envoyer</button>     
                    </div>
                </div>    
                </form>
            </div>
        </div>
    </div>        
</section>
  
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection