@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection


@section('content')

<div class="container-fluid">
@include('partials.new_site._new_site_nav')
<section class="">
    <div class="row"> 
        <div class="col">
            <div class="card content">
                 @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-header">
                    <h5 class="text-center">Ajouter un ouvrier</h5>
                </div>
                <form method="POST" action="{{ route('ouvriers.store' )}}" data-parsley-validate="">
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                @csrf
                <div class="card-body">
                    <div class="form-group row">
                        <label for="metier_ouvrier" class="col-sm-3 col-form-label">Métier</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="metier_ouvrier" name="metier_ouvrier" placeholder="Obligatoire" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="employeur_ouvrier" class="col-sm-3 col-form-label">Employeur</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" id="employeur_ouvrier" name="employeur_ouvrier">
                        </div>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="sous_traitant_ouvrier" value=1 name="sous_traitant_ouvrier">
                        <label class="custom-control-label" for="sous_traitant_ouvrier">Sous-traitant</label>
                    </div>
                </div>    
                <div class="card-footer">
                    <div class="form-group row">
                    <button type="submit" class="col-sm-8 offset-sm-2 btn btn-success btn-block btn-sub">Ajouter</button>  
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection