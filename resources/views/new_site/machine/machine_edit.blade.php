@extends('layouts.app')

{{--  Validation client --}}
@section('stylesheet')

    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">

@endsection


@section('content')

<div class="container-fluid">
@include('partials.new_site._new_site_nav')
<section class="">
    <div class="row"> 
        <div class="col">
            <div class="card content">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-header">
                    <h5 class="text-center">Modifier votre machine</h5>
                </div>
            {!! Form::model($machine, ['route' => ['machines.update', $machine->id],'data-parsley-validate'=>'','method'=>'PUT']) !!} 
            <div class="card-body">
                {{Form::label('type_machine', 'Type:')}}
                {{Form::text('type_machine', null, ['class'=>'form-control', 'required'=>'', 'placeholder'=>'Obligatoire'])}}

                {{Form::label('marque_machine', 'Marque:')}}
                {{Form::text('marque_machine', null, ['class'=>'form-control'])}}

                {{Form::label('proprietaire_machine', 'Proprietaire:')}}
                {{Form::text('proprietaire_machine', null, ['class'=>'form-control', 'required'=>'', 'placeholder'=>'Obligatoire'])}}

                {{Form::label('immatriculation_machine', 'Immatriculation:')}}
                {{Form::text('immatriculation_machine', null, ['class'=>'form-control', 'required'=>'', 'placeholder'=>'Obligatoire'])}}

                {{Form::label('sous_traitant_machine', 'Soutraitant:')}}
                {{Form::checkbox('sous_traitant_machine', '1', ['class'=>'form-control'])}}                
            </div>
            <div class="card-footer">
                <a href="{{ route('machines.index', $machine->id )}}" class="btn btn-primary">Annuler</a>
                {{Form::submit('Sauvegarder les changements', ['class'=>'btn btn-success'])}}
            </div>
            {{Form::close()}}
            </div>
        </div>
    </div>
</section>
  
</div>
@endsection

{{--  Validation client, Date  --}}
@section('scripts')

<script src="{{ asset('js/parsley.js') }}"></script>

@endsection