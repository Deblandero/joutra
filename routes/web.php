<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/principale', 'PrincipaleController@principale')->name('principale');
Route::get('/nouveau-chantier', 'HomeController@new_site')->name('principale');

    //*************************** */
    //    NEW SITE                //
    //*************************** */

Route::resource('/nouveau-chantier/parametre', 'NewSiteSetController');

Route::resource('/nouveau-chantier/ouvriers', 'NewSiteBobController');

Route::resource('/nouveau-chantier/machines', 'NewSiteMachineController');

Route::resource('/nouveau-chantier/invitations', 'NewSiteInvitationController');

    //*************************** */
    //    PDF JOURNALIER          //
    //*************************** */

Route::resource('/fiche-journaliere', 'FicheJournaliereController');

Route::resource('/pdf-fiche-journaliere', 'PdfJournaliereController');
