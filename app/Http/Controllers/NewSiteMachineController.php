<?php

namespace App\Http\Controllers;

use App\NewSiteMachine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;


class NewSiteMachineController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $machines = NewSiteMachine::where('user_id', Auth::user()->id)->get();
        return view('new_site.machine.machine_index')->withMachines($machines);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('new_site.machine.machine_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'type_machine' => 'required|string',
            'marque_machine' => 'nullable|string',
            'proprietaire_machine' => 'nullable|string',
            'immatriculation_machine' => 'required|string',
        ));

        $newsitemachine = new NewSiteMachine;

        $newsitemachine->type_machine = $request->type_machine;
        $newsitemachine->marque_machine = $request->marque_machine;
        $newsitemachine->proprietaire_machine = $request->proprietaire_machine;
        $newsitemachine->immatriculation_machine = $request->immatriculation_machine;
        $newsitemachine->sous_traitant_machine = $request->sous_traitant_machine;
        $newsitemachine->user_id = $request->user_id;

        $newsitemachine->save();

        return redirect()->route('machines.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $machine = NewSiteMachine::find($id);
        return view('new_site.machine.machine_show')->withMachine($machine);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $machine = NewSiteMachine::find($id);
        return view('new_site.machine.machine_edit')->withMachine($machine);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'type_machine' => 'required|string',
            'marque_machine' => 'nullable|string',
            'proprietaire_machine' => 'required|string',
            'immatriculation_machine' => 'required|string',
            'sous_traitant_machine' => 'nullable|int'
        ));

        $machine = NewSiteMachine::find($id);

        $machine->type_machine = $request->input('type_machine');
        $machine->marque_machine = $request->input('marque_machine');
        $machine->proprietaire_machine = $request->input('proprietaire_machine');
        $machine->immatriculation_machine = $request->input('immatriculation_machine');
        $machine->sous_traitant_machine = $request->input('sous_traitant_machine');

        $machine->save();

        Session::flash('success', 'Vous avez bien modifié votre machine');

        return redirect()->route('machines.show', $machine->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $machine = NewSiteMachine::find($id);

        $machine->delete();

        Session::flash('success', 'Votre machine a bien été supprimée');

        return redirect()->route('machines.index');
    }
}
